FROM golang:1.8-alpine AS build

RUN apk add -U git jq
RUN go get golang.org/x/tools/cmd/goimports
RUN cd $GOPATH/src/golang.org/x/tools/cmd/goimports && git describe --tag --always > /goimports_version

WORKDIR /go/src/gitlab.com/nolith/codeclimate-goimports
COPY . .

RUN go build

RUN cat engine.json | jq ".version = \"$(cat /goimports_version)-$(git describe --tag --always)\"" > /engine.json


FROM alpine:3.6

LABEL maintainer="Alessio Caiazza" 

RUN adduser -u 9000 -D app
COPY --from=build /go/bin/goimports /usr/bin/goimports
COPY --from=build /go/src/gitlab.com/nolith/codeclimate-goimports/codeclimate-goimports /usr/bin/codeclimate-goimports
COPY --from=build /engine.json /engine.json

WORKDIR /code
VOLUME /code

USER app

CMD ["/usr/bin/codeclimate-goimports"]
