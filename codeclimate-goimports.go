package main

import (
	"fmt"
	"os"
	"os/exec"
	"strconv"
	"strings"

	"github.com/codeclimate/cc-engine-go/engine"
	"sourcegraph.com/sourcegraph/go-diff/diff"
)

const (
	defaultMaxComplexity = 9
	complexityField      = 0
	packageField         = 1
	functionField        = 2
	locationField        = 3
	fieldsCount          = 4
	locationFileField    = 0
	locationRowField     = 1
	locationColumnField  = 2
	locationFieldsCount  = 3
)

func main() {
	rootPath := "/code/"

	config, err := engine.LoadConfig()
	if err != nil {
		fmt.Fprintf(os.Stderr, "Error loading config: %s", err)
		os.Exit(1)
	}

	analysisFiles, err := engine.GoFileWalk(rootPath, engine.IncludePaths(rootPath, config))
	if err != nil {
		fmt.Fprintf(os.Stderr, "Error initializing: %s", err)
		os.Exit(1)
	}

	localPackageName := getLocalPackageName(config)

	for _, path := range analysisFiles {
		relativePath := strings.SplitAfter(path, rootPath)[1]

		lintFile(relativePath, localPackageName)
	}
}

func lintFile(path, localPackageName string) {
	goimports := exec.Command("goimports", "-d", "-e", "-local", localPackageName, path)
	out, err := goimports.CombinedOutput()
	if err != nil {
		fmt.Fprintf(os.Stderr, "Error analyzing path: %v\n", path)
		fmt.Fprintf(os.Stderr, "Error: %v\n", err)

		if out != nil {
			s := string(out[:])
			fmt.Fprintf(os.Stderr, "Goimports output: %v\n", s)
		}

		os.Exit(1)
	}

	diffs, err := diff.ParseMultiFileDiff(out)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Error parsing diff for output: %v\n", out)
		fmt.Fprintf(os.Stderr, "\nError: %v\n", err)
		os.Exit(1)
	}

	if diffs != nil && diffs[0] != nil && len(diffs[0].Hunks) > 0 {
		numHunks := len(diffs[0].Hunks)

		description := "Your code does not pass goimports in " + strconv.Itoa(numHunks) + " " + pluralizePlace(numHunks)

		issue := &engine.Issue{
			Type:              "issue",
			Check:             "GoImports/Style/GoImports",
			Description:       description,
			RemediationPoints: int32(50000 * numHunks),
			Categories:        []string{"Style"},
			Location: &engine.Location{
				Path: path,
				Lines: &engine.LinesOnlyPosition{
					Begin: 1,
					End:   1,
				},
			},
		}
		engine.PrintIssue(issue)
	}
}

func getLocalPackageName(config engine.Config) string {
	if subConfig, ok := config["config"].(map[string]interface{}); ok {
		if local, ok := subConfig["local"].(string); ok {
			return local
		}
	}

	return ""
}

func pluralizePlace(quant int) string {
	if quant > 1 {
		return "places"
	}
	return "place"
}
