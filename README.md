# Code Climate GoImports Engine

`codeclimate-goimports` is a Code Climate engine that wraps [Goimports](https://godoc.org/golang.org/x/tools/cmd/goimport). You can run it on your command line using the Code Climate CLI, or on our hosted analysis platform.

Goimports checks your Go import lines, warning about missing and unreferenced ones.

### Installation

1. If you haven't already, [install the Code Climate CLI](https://github.com/codeclimate/codeclimate).
2. Run `codeclimate engines:enable goimports`. This command both installs the engine and enables it in your `.codeclimate.yml` file.
3. You're ready to analyze! Browse into your project's folder and run `codeclimate analyze`.

### Configuration

Like the `goimports` binary, you can configure the local package in order to enforce proper
import separation. `stdlib` -> `3rd party` -> `local`.
The default value is empty and local imporst will be mixed with 3rd party libs;
you can configure your own threshold in your `.codeclimate.yml`:

```yaml
version: "2"
plugins:
  goimports:
    enabled: true
    config:
      local: gitlab.com/nolith/codeclimate-goimports
```

### Building

```console
docker build -t codeclimate/codeclimate-goimports .
```
